<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>Tambah Mata Kuliah</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        header {
            background-color: #333;
            color: #fff;
            text-align: center;
            padding: 20px;
        }

        h1 {
            margin: 0;
        }

        .container {
            max-width: 800px;
            margin: 20px auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table,
        th,
        td {
            border: 1px solid #ddd;
        }

        td {
            padding: 12px;
            text-align: left;
        }

        th {
            padding: 12px;
            background-color: #333;
            color: #fff;
            text-align: center;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        a {
            text-decoration: none;
            color: #007bff;
        }

        a:hover {
            text-decoration: underline;
        }

        button {
            background-color: #333;
            color: #fff;
            border: none;
            padding: 6px 12px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 0;
            cursor: pointer;
            border-radius: 4px;
        }

        .tambah {
            background-color: #333;
            color: #fff;
            border: none;
            padding: 6px 12px;
            justify-content: center;
            align-items: center;
            display: inline-block;
            margin: 0;
            cursor: pointer;
            border-radius: 4px;
            margin-left: 15%;
        }

        .search-bar {
            justify-content: center;
            margin: 20px auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        .kolom-sks, .kolom-kelas, .kolom-isi-kelas {
            justify-content: center;
            align-items: center;
            margin-left: 35%;
            text-align: center;
        }
    </style>
</head>

<body>
    <header>
        <h1>Kartu Rencana Studi (KRS)</h1>
    </header>
    <div class="container">
        <div class="search-bar">
            <form action="c_search_mk_krs.php" method="post">
                <label>Kode mata kuliah: </label>
                <input type="text" name="kode_mk" placeholder="Masukkan kode mata kuliah">
                <button type="submit" name="kirim_cari_kode_mk">Cari</button>
            </form>
        </div>

        <table>
            <tr>
                <th>Kode</th>
                <th>Nama Mata Kuliah</th>
                <th>SKS</th>
                <th>Kelas</th>
                <th>Isi Kelas</th>
                <th>Hari</th>
                <th>Jam</th>
                <th>Action</th>
            </tr>
            <?php
            if (isset($jadwal)) {
                foreach ($jadwal as $nilai) {
                    echo "<tr>";
                    echo "<td>$nilai[kode_mk]</td>";
                    echo "<td>$nilai[nama_mk]</td>";
                    echo "<td class='kolom-sks'>$nilai[sks]</td>";
                    echo "<td class='kolom-kelas'>$nilai[kelas]</td>";
                    echo "<td class='kolom-isi-kelas'>$nilai[isi_kelas]</td>";
                    echo "<td>$nilai[hari]</td>";
                    echo "<td>$nilai[jam]</td>";
                    echo "<td><form action='c_tambah.php' method='post'>
                    <input type='hidden' name='id_mk' value=$nilai[id_mk]>
                    <input class='tambah' type='submit' name='kirim_tambah_mk' value='+'>
                </form></td>";
                    echo "</tr>";
                }
            }
            ?>
        </table>
    </div>
</body>

</html>
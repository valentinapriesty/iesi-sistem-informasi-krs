<!DOCTYPE html>
<html>

<head>
    <title>Kartu Rencana Studi</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        header {
            background-color: #333;
            color: #fff;
            text-align: center;
            padding: 20px;
        }

        h1 {
            margin: 0;
        }

        .container {
            max-width: 800px;
            margin: 20px auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
            place-items: center;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table,
        th,
        td {
            border: 1px solid #ddd;
        }

        td {
            padding: 12px;
            text-align: left;
        }

        th {
            padding: 12px;
            background-color: #333;
            color: #fff;
            text-align: center;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        a {
            text-decoration: none;
            color: #007bff;
        }

        a:hover {
            text-decoration: underline;
        }

        button {
            background-color: #333;
            color: #fff;
            border: none;
            padding: 6px 12px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 0;
            cursor: pointer;
            border-radius: 4px;
            justify-content: center;
            margin-left: 40%;
        }

        .hapus {
            background-color: #333;
            color: #fff;
            border: none;
            padding: 6px 12px;
            justify-content: center;
            align-items: center;
            display: inline-block;
            margin-left: 35%;
            cursor: pointer;
            border-radius: 4px;
        }

        .kolom-kelas {
            justify-content: center;
            align-items: center;
            margin-left: 35%;
            text-align: center;
        }
    </style>
</head>

<body>
    <header>
        <h1>Kartu Rencana Studi (KRS)</h1>
    </header>
    <div class="container">
        <a href="v_tambah.php"><button>Tambah Mata Kuliah</button></a>
        <br>
        <table>
            <tr>
                <th>Kode</th>
                <th>Nama Mata Kuliah</th>
                <th>Kelas</th>
                <th>Action</th>
            </tr>
            <?php
                foreach ($matkul as $nilai) {
                    echo "<tr>";
                    echo "<td>$nilai[kode_mk]</td>";
                    echo "<td>$nilai[nama_mk]</td>";
                    echo "<td class='kolom-kelas'>$nilai[kelas]</td>";
                    echo "<td>
                    <form action='c_hapus.php' method='post'>
                    <input type='hidden' name='id_krs' value=$nilai[id_krs]>
                    <input class='hapus' type='submit' name='kirim_hapus_mk' value='-'>
                    </form>
                    </td>";
                    echo "</tr>";
                }
            ?>
        </table>
    </div>
</body>

</html>
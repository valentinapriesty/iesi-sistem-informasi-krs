<!DOCTYPE html>
<html>
<head>
    <title>Kartu Rencana Studi</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        header {
            background-color: #333;
            color: #fff;
            text-align: center;
            padding: 20px;
        }

        h1 {
            margin: 0;
        }

        .container {
            max-width: 800px;
            margin: 20px auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid #ddd;
        }

        th, td {
            padding: 12px;
            text-align: left;
        }

        th {
            background-color: #333;
            color: #fff;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        a {
            text-decoration: none;
            color: #007bff;
        }

        a:hover {
            text-decoration: underline;
        }

        .cancel-button {
            background-color: #ff0000;
            color: #fff;
            border: none;
            padding: 6px 12px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 0;
            cursor: pointer;
            border-radius: 4px;
        }

        .cancel-button:hover {
            background-color: #cc0000;
        }

        /* CSS untuk form penambahan mata kuliah */
        form {
            margin-top: 20px;
        }

        label {
            display: block;
            margin-bottom: 5px;
        }

        input[type="text"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        input[type="submit"] {
            background-color: #007bff;
            color: #fff;
            border: none;
            padding: 10px 20px;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <header>
        <h1>Kartu Rencana Studi (KRS)</h1>
    </header>
    <div class="container">
        <!-- Form untuk penambahan mata kuliah -->
        <form action="tambah.php" method="post">
            <label for="kode_mk">Kode MK:</label>
            <input type="text" name="kode_mk" required><br>

            <label for="nama_mk">Nama MK:</label>
            <input type="text" name="nama_mk" required><br>

            <label for="kelas">Kelas:</label>
            <input type="text" name="kelas" required><br>
            <input type="submit" name="tambah_mata_kuliah" value="Tambah Mata Kuliah">

        </form>

        <table>
            <tr>
                <th>Kode</th>
                <th>Nama Mata Kuliah</th>
                <th>Kelas</th>
                <th>Hari</th>
                <th>Jam</th>
                <th>Action</th>
            </tr>
            <?php
            foreach($matkul as $nilai){
                echo "<tr>";
                echo "<td>$nilai[kode_mk]</td>";
                echo "<td>$nilai[nama_mk]</td>";
                echo "<td>$nilai[kelas]</td>";
                echo "<td>$nilai[hari]</td>";
                echo "<td>$nilai[jam]</td>";
                echo "<td><form action='hapus.php' method='post'>
                <input type='hidden' name='id_mk' value=$nilai[id_mk]>
                <input type='submit' name='kirim' value='Hapus'>
                </form>
                </td>";
                echo "</tr>";
            }
            ?>
        </table>
    </div>
</body>
</html>

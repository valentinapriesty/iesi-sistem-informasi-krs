<?php
require "./koneksiMVC.php";
class m_krs{
    private $database;
    protected $tablename = "mata_kuliah";
    public function __construct(){
        $this->database = new koneksiMVC();
        $this->database = $this->database->mysqli;
        
    }

    // Get All program kerja
    public function getAll(){
    return $this->database->query("SELECT id_mk, kode_mk, nama_mk, kelas, hari, jam FROM $this->tablename");
    }

    //Delete Program Kerja
    public function deleteKrs($id_mk){
        $this->database->query(
        "DELETE FROM $this->tablename WHERE id_mk = '$id_mk'");
    }

    public function addKrs($kode_mk, $nama_mk, $kelas, $hari, $jam){
    $this->database->query("INSERT INTO $this->tablename (kode_mk, nama_mk, kelas, hari, jam) 
                            VALUES ('$kode_mk', '$nama_mk', '$kelas', '$hari', '$jam')");
}

    public function updateKrs($kode_mk,$nama_mk,$jam){
        $this->database->query("UPDATE $this->tablename SET nama_mk = '$nama_mk', jam = '$jam'
        WHERE kode_mk = '$kode_mk'");
    }

    public function selectedCourses($id_mk) {
        $data= $this->database->getSelectedCourses($id_mk);

        // Load view untuk menampilkan data mata kuliah yang sudah dipilih
        $this->load->view('tambah', $data);
    }

    }

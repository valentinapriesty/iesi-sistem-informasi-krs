<?php
include_once 'm_krs.php';
class c_krs{
    public $model;
    public function __construct(){
        $this->model = new m_krs();
    }
    public function invoke(){
        $matkul = $this->model->getAll();
        include 'v_krs.php';
    }
    public function add($kode_mk,$nama_mk,$kelas,$hari,$jam){
        $this->model->setKrs($kode_mk,$nama_mk,$kelas,$hari,$jam);
    }
    public function delete($id_mk){
        $this->model->deleteKrs($id_mk);
    }
    public function update($kode_mk,$nama_mk,$jam){
        $this->model->updateKrs($kode_mk,$nama_mk,$jam);
    }

    public function getSelectedData(){
        $data = $this->model->getAll();
        include 'tambah.php';
    }
    
}